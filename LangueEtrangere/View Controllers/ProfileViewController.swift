//
//  ProfileViewController.swift
//  LangueEtrangere
//
//  Created by Nikita Shkarupa on 07/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var userPanel: UIView!
    @IBOutlet weak var userPoints: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var table: UITableView!
    
    let personService = PersonService()
    let courseService = CourseService()
    
    let tableSections = [
        "Purchased courses",
        "Finished courses"
    ]
    
    var purchasedCourses: [Course?] = []
    var finishedCourses: [Course?] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        
        table.tableFooterView = UIView()
        table.delegate = self
        table.dataSource = self
        
        let logoutBtn = UIBarButtonItem(title: "Sign Out", style: .plain, target: self, action: #selector(logout))
        logoutBtn.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont(name: "Helvetica Neue", size: 18)!,
            NSAttributedString.Key.foregroundColor : UIColor.red],
            for: UIControl.State.normal)
        
        self.navigationItem.rightBarButtonItem = logoutBtn
        
        userPanel.layer.shadowColor = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0).cgColor
        userPanel.layer.shadowOffset = CGSize(width: 0.0, height: 0.25)
        userPanel.layer.shadowOpacity = 1.0
        userPanel.layer.shadowRadius = 0.0
        
        imgUser.layer.cornerRadius = imgUser.frame.width / 2
        
        loadUserInfo()
        loadCourses()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(changeAvatar))
        imgUser.addGestureRecognizer(tap)
        imgUser.isUserInteractionEnabled = true
    }
    
    func loadCourses() {
        let personId = UserDefaults.standard.integer(forKey: "userID")
        courseService.getCourses(personId: personId, available: false, counter: true) { (userCourses, error) in
            self.purchasedCourses = userCourses
            DispatchQueue.main.async {
                self.table.reloadData()
            }
        }
    }
    
    func loadUserInfo() {
        let userID = UserDefaults.standard.integer(forKey: "userID")
        personService.getPersonById(id: userID, answeredQuestions: true) { (user, error) in
            DispatchQueue.main.async {
                self.userName.text = user!.name!
                self.userPoints.text = "Beginner - \(user!.answeredQuestions) points recived"
            }
        }
    }
    
    @objc func changeAvatar() {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: "Update your avatar", message: "Choose new avatar sourse", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Unable to use camera", message: "Can't use camera on your device :(", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in

            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                imagePicker.sourceType = .photoLibrary
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Unable to use library", message: "Can't use library on your device :(", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cansel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[.originalImage] as? UIImage
        
        self.imgUser.image = image
        self.imgUser.contentMode = .scaleAspectFill
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @objc func logout() {
        UserDefaults.standard.removeObject(forKey: "userID")
        print("User id after logout: \(UserDefaults.standard.integer(forKey: "userID"))")
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? CourseDetailsViewController {
            let selectedCourse = self.table.indexPathForSelectedRow?.row
            destinationViewController.course = self.table.indexPathForSelectedRow?.section == 0 ? purchasedCourses[selectedCourse!] : finishedCourses[selectedCourse!]
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadCourses()
        loadUserInfo()
    }
}

extension ProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = section == 0 ? purchasedCourses.count : finishedCourses.count
        if numberOfRows == 0 {
            return 1
        }
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! UITableViewCell
        
        if indexPath.section == 0 { // purchased courses
            
            cell.textLabel?.text = purchasedCourses.count == 0 ? "No purchased courses yet." : purchasedCourses[indexPath.row]?.language
            
        } else if indexPath.section == 1 { // finished courses
            cell.textLabel?.text = finishedCourses.count == 0 ? "No finished courses yet." : finishedCourses[indexPath.row]?.language
        }
        
        if cell.textLabel?.text == "No purchased courses yet." || cell.textLabel?.text == "No finished courses yet." {
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.isUserInteractionEnabled = false
            cell.textLabel?.isEnabled = false
            
        } else {
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.default
            cell.isUserInteractionEnabled = true
            cell.textLabel?.isEnabled = true
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.textAlignment = .center
        label.text = tableSections[section]
        label.backgroundColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 41
    }
    
}
