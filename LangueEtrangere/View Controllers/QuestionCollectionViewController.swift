import UIKit

private let reuseIdentifier = "Cell"

protocol QuestionProtocol {
    func answerQuesdtion(question: Question, theme: Theme)
}

class QuestionCollectionViewController: UICollectionViewController {
    
    let questionService = QuestionService()
    let personDataService = PersonDataService()
    
    var course: Course?
    var theme: Theme?
    var checkedQuestions: [Question?] = []
    var question: Question?
    var currentQuestionIndex = 0
    
    var delegate: QuestionProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        navigationController?.navigationBar.prefersLargeTitles = false
        
        questionService.getQuestionsByThemeId(
            personId: UserDefaults.standard.integer(forKey: "userID"),
            courseId: course!.id!,
            themeId: theme!.id!) { (res, err) in
                self.checkedQuestions = res
                self.nextQuestion()
        }
    }


    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return question?.options.count ?? 0
    }
    
    
    func nextQuestion() {

        var showAnimation = false

        if question != nil {
            showAnimation = true
            delegate.answerQuesdtion(question: question!, theme: theme!)
            checkedQuestions.first(where: { $0?.id == question?.id })?!.answered = true
            
            self.personDataService.addQuestion(
                personId: UserDefaults.standard.integer(forKey: "userID"),
                courseId: course!.id!,
                themeId: theme!.id!,
                questionId: question!.id!) { (res, err) in }
        }

        while checkedQuestions.count > currentQuestionIndex  {
            let currentQuestion = checkedQuestions[currentQuestionIndex]
            
            if !currentQuestion!.answered {
                self.question = currentQuestion
                if showAnimation {
                    UIView.transition(
                        with: collectionView,
                        duration: 0.7,
                        options: .transitionCurlDown,
                        animations: { self.collectionView.reloadData() })
                }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                return
            }
            currentQuestionIndex += 1
        }
        self.outOfQuestions()
    }
    
    
    func outOfQuestions() {
        let modal = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "Congradulations") as! ModalViewController
        self.addChild(modal)
        modal.view.frame = self.view.frame
        self.view.addSubview(modal.view)
        modal.didMove(toParent: self)
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CollectionHeader", for: indexPath) as! QuestionHeadingReusableView
            
            view.questionText.text = self.question?.type
            view.questionWord.text = self.question?.question
            view.questionTip.text = self.question?.tip

            return view
        }
        fatalError("Unexpected kind")
    }


    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestionCell", for: indexPath) as! QuestionCollectionViewCell
    
        let option = question?.options[indexPath.item]
        cell.optionIcon.image = UIImage(named: option!.image!)
        cell.optionText.text = option!.text!
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedOption = question?.options[indexPath.item]
        
        if (selectedOption!.correct!) {
            
            self.nextQuestion()
            
        } else {
            let alert = UIAlertController(
                title: "Your answer is incorrect. Focus and try one more time!",
                message: nil,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }

}
