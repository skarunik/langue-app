//
//  FinishedTopicViewController.swift
//  LangueEtrangere
//
//  Created by Nikita Shkarupa on 14/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import UIKit

class FinishedTopicViewController: UIViewController {
    
    var course: Course?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destinationViewController = segue.destination as? ThemesTableViewController
        else {
            return
        }
        destinationViewController.course = course
        destinationViewController.hidesBottomBarWhenPushed = false
        destinationViewController.navigationController?.isNavigationBarHidden = false
    }
}
