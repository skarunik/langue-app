import UIKit

private let reuseIdentifier = "Cell"

protocol NewCourseProtocol {
    func addPurchasedCourse(course: Course)
}

class NewCourseCollectionViewController: UICollectionViewController {
    
    let courseService = CourseService()
    let personDataService = PersonDataService()
    
    var availableCourses: [Course] = []
    
    var delegateCourses: NewCourseProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        let userID = UserDefaults.standard.integer(forKey: "userID")
        courseService.getCourses(personId : userID, available : true, counter : false) {(courses, error) in
            self.availableCourses = courses as! [Course]
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return availableCourses.count
    }
    

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CourseCell", for: indexPath) as! NewCourseCollectionViewCell
        cell.courseTitle.text = availableCourses[indexPath.row].language!
        cell.courseFlag.image = UIImage(named: availableCourses[indexPath.row].flag!)
        
        var themesCount = 0
        for theme in availableCourses[indexPath.row].themes {
            themesCount += (theme?.questions.count)!
        }
        let coursePrice = availableCourses[indexPath.row].price
        cell.themesCount.text = String("\(themesCount) questions")
        cell.coursePrice.text = "\(coursePrice!) $"
        
        return cell
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedCourse = availableCourses[indexPath.item]
        
        let alert = UIAlertController(
            title: "Verification Required",
            message: "Are you sure you want to purchase this item: \"\(selectedCourse.language!) course\"?",
            preferredStyle: UIAlertController.Style.alert
        )
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Purchase", style: .default, handler: { _ in
            let userId = UserDefaults.standard.integer(forKey: "userID")
            self.purcaseCourse(userId: userId, courseId: selectedCourse.id!, indexPath: indexPath)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func purcaseCourse(userId: Int, courseId: Int, indexPath: IndexPath) {
        let courseId = self.availableCourses[indexPath.item].id!
        let personId = UserDefaults.standard.integer(forKey: "userID")

        self.personDataService.addCourse(personId: personId, courseId: courseId) { (bool, err) in }
        
        self.delegateCourses.addPurchasedCourse(course: self.availableCourses[indexPath.item])
        
        self.availableCourses.remove(at: indexPath.item)
        self.collectionView?.deleteItems(at: [indexPath])
    }

}
