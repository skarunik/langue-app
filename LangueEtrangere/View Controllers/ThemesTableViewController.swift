//
//  ThemesTableViewController.swift
//  LangueEtrangere
//
//  Created by Nikita Shkarupa on 08/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import UIKit

class ThemesTableViewController: UITableViewController, QuestionProtocol {
    
    let themeService = ThemeService()
    var course : Course?
    var themes : [Theme?] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = course?.language
        
        let courseLevel = 1
        let level = UIBarButtonItem(title: "Level \(courseLevel)", style: .plain, target: self, action: nil)
        level.isEnabled = false
        navigationItem.rightBarButtonItem = level
        
        tableView.tableFooterView = UIView()
        hidesBottomBarWhenPushed = false
        
        loadThemes()
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return themes.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ThemeTableViewCell", for: indexPath) as? ThemeTableViewCell else {
            fatalError("The dequeued cell is not an instance of ThemeTableViewCell.")
        }
        
        let theme = themes[indexPath.row]
        
        let answeredQuestions = theme?.answeredQuestionsCounter
        let totalQuestions = theme?.questions.count
        cell.themeName.text = theme!.name
        cell.themeProgress.text = "\(answeredQuestions ?? 0)/\(totalQuestions ?? 0)"
        cell.themeIcon.image = UIImage(named: theme!.icon!)
        cell.themeProgressBar.progress = Float(answeredQuestions!) / Float(totalQuestions!)
        
        if totalQuestions == answeredQuestions {
            cell.isUserInteractionEnabled = false
            cell.themeName.textColor = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
        }
        
        return cell
    }
    
    
    func loadThemes() {
        let userID = UserDefaults.standard.integer(forKey: "userID")
        themeService.getThemesByCourseId(personId: userID, courseId: course!.id!, counter: true) { (themes, error) in
            self.themes = themes
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? QuestionCollectionViewController {
            let selectedTheme = tableView.indexPathForSelectedRow?.row
            
            destinationViewController.course = course
            destinationViewController.theme = themes[selectedTheme!]
            destinationViewController.hidesBottomBarWhenPushed = true
            
            destinationViewController.delegate = self
        }
    }
    
    
    func answerQuesdtion(question: Question, theme: Theme) {
        self.themes.first(where: { $0?.id == theme.id })?!.answeredQuestionsCounter += 1
        self.themes.first(where: { $0?.id == theme.id })?!.questions.first(where: { $0?.id == question.id })?!.answered = true
        self.tableView.reloadData()
    }

}
