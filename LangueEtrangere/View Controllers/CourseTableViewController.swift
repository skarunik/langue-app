import UIKit

class CourseTableViewController: UITableViewController, NewCourseProtocol {
    
    var courseService = CourseService()
    var courses : [Course?] = []
    var selectedCourseIndex = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = 90
        tableView.tableFooterView = UIView()
        navigationController?.navigationBar.prefersLargeTitles = true
        
        loadCourses()
    }

    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CourseTableViewCell", for: indexPath) as? CourseTableViewCell else {
            fatalError("The dequeued cell is not an instance of CourseTableViewCell.")
        }
        
        let course = courses[indexPath.row]
        
        cell.courseName.text = course!.language
        cell.courseLevel.text = "Level 1"
        cell.courseProgress.text = "\(course?.themes.count ?? 0) topics"
        cell.courseFlag.image = UIImage(named: course!.flag!)
        
        return cell
    }
    
    
    private func loadCourses() {

        let userID = UserDefaults.standard.integer(forKey: "userID")
        courseService.getCourses(personId: userID, available: false, counter: true) { (userCourses, error) in
            self.courses = userCourses
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCourseIndex = indexPath.row
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationViewController = segue.destination as? ThemesTableViewController {
            let selectedCourse = tableView.indexPathForSelectedRow?.row
            destinationViewController.course = courses[selectedCourse!]
        }
        
        if let destinationViewController = segue.destination as? NewCourseCollectionViewController {
            destinationViewController.delegateCourses = self
        }
    }
    
    
    func addPurchasedCourse(course: Course) {
        self.courses.append(course)
        self.tableView.reloadData()
    }

}
