//
//  LoginViewController.swift
//  LangueEtrangere
//
//  Created by Nikita Shkarupa on 05/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    let personService = PersonService()

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var lognBtn: UIButton!
    @IBOutlet weak var panel: UIView!
    @IBOutlet weak var loginError: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.lognBtn.layer.cornerRadius = 4
        self.email.layer.cornerRadius = 4
        self.password.layer.cornerRadius = 4
        self.panel.layer.cornerRadius = 4
        
    }
    
    @IBAction func login(_ sender: UIButton) {
        
        view.endEditing(true)
        loginError.isHidden = true
        
        personService.login(email: email.text!, password: password.text!) {(personId, error) in
            if error != nil || personId == nil {
                self.loginFailed()
                return
            }

            UserDefaults.standard.set(personId, forKey: "userID")
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
            self.present(vc, animated: true, completion: nil)
        }
        
    }

    @IBAction func emailReturn(_ sender: UITextField) {
        view.endEditing(true)
    }
    
    
    @IBAction func passwordReturn(_ sender: UITextField) {
        view.endEditing(true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        loginError.isHidden = true
        view.reloadInputViews()
    }
    
    
    func loginFailed() {
        DispatchQueue.main.async {
            self.loginError.isHidden = false
        }
    }
    
}
