//
//  CourseDetailsViewController.swift
//  LangueEtrangere
//
//  Created by Nikita Shkarupa on 17/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import UIKit

class CourseDetailsViewController: UIViewController {

    @IBOutlet weak var courseName: UILabel!
    @IBOutlet weak var courseDescription: UITextView!
    @IBOutlet weak var myRate: UILabel!
    @IBOutlet weak var courseImage: UIImageView!
    @IBOutlet weak var courseRateBar: UIProgressView!
    
    var course: Course?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        courseName.text = course?.language
        courseDescription.text = course?.courseDescription
        courseImage.image = UIImage(named: course!.flag!)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        
        gradientLayer.colors = [UIColor.black.withAlphaComponent(0).cgColor, UIColor.black.withAlphaComponent(1).cgColor]
        courseImage.layer.addSublayer(gradientLayer)
        
        courseRateBar.progress = Float(course!.rating / 5)
        myRate.text = String(course!.rating)
        courseRateBar.transform = CGAffineTransform(scaleX: 1, y: 4)
    }
}
