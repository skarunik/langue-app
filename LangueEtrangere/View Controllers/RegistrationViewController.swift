//
//  RegistrationViewController.swift
//  LangueEtrangere
//
//  Created by Nikita Shkarupa on 19/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    
    let personService = PersonService()
    
    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var emailInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var panel: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameInput.layer.cornerRadius = 4
        passwordInput.layer.cornerRadius = 4
        emailInput.layer.cornerRadius = 4
        panel.layer.cornerRadius = 4
        createButton.layer.cornerRadius = 4
        
        errorLabel.isHidden = true
    }
    
    
    @IBAction func createAccount(_ sender: Any) {
        view.endEditing(true)
        self.errorLabel.isHidden = true
        
        if emailInput.text == "" || nameInput.text == "" || passwordInput!.text == "" {
            self.registrationError()
            return
        }
        
        personService.registration(
            email: emailInput.text!,
            name: nameInput.text!,
            password: passwordInput.text!) { (res, err) in
                
            if res {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.registrationError()
                return
            }
        }
    }
    
    
    func registrationError() {
        DispatchQueue.main.async {
            self.errorLabel.isHidden = false
        }
    }
    
}
