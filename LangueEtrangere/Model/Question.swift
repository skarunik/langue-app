//
//  Question.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 06/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class Question: NSObject, Codable {
    
    let id : Int?
    let type : String?
    let question : String?
    let tip : String?
    let options : [Option?]
    var answered : Bool = false
    
    private enum CodingKeys: String, CodingKey {
        case id
        case type
        case question
        case tip
        case options
    }
    
}
