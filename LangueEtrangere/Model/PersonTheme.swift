//
//  PersonTheme.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 08/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation


class PersonTheme: NSObject, Codable {
    
    let id : Int?
    var answeredQuestions : [AnsweredQuestion?]
    
    init(id : Int) {
        self.id = id
        self.answeredQuestions = []
    }
    
}
