//
//  Option.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 06/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class Option: NSObject, Codable {
    
    let text : String?
    let image : String?
    let correct : Bool?
    
}
