//
//  Person.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 06/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class Person : NSObject, Codable {
    
    let id : Int?
    let email : String?
    let name : String?
    let password : String?
    var courses : [PersonCourse?]
    var answeredQuestions : Int = 0
    
    private enum CodingKeys: String, CodingKey {
        case id
        case email
        case name
        case password
        case courses
    }
    
    init(email : String, name : String, password : String) {
        self.id = nil
        self.email = email
        self.name = name
        self.password = password
        self.courses = []
    }
    
}
