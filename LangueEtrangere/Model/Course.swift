//
//  Course.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 06/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class Course: NSObject, Codable {
    
    let id : Int?
    let language : String?
    let courseDescription : String?
    let flag : String?
    let price : Double?
    let themes : [Theme?]
    var ratings : [Rating?]
    var startedThemesCounter : Int = 0
    var rating : Float = 0
    
    private enum CodingKeys: String, CodingKey {
        case id
        case language
        case courseDescription
        case flag
        case themes
        case ratings
        case price
    }
    
}
