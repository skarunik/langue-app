//
//  Rating.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 06/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class Rating: NSObject, Codable {
    
    let userId : Int?
    var rate : Int?
    
    init(userId : Int, rate : Int) {
        self.userId = userId
        self.rate = rate
    }
    
}
