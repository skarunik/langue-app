//
//  PersonCourse.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 08/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class PersonCourse: NSObject, Codable {
    
    let id : Int?
    var themes : [PersonTheme?]
    
    init(id : Int) {
        self.id = id
        self.themes = []
    }
    
}
