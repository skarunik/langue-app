//
//  AnsweredQuestion.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 08/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class AnsweredQuestion: NSObject, Codable {
    
    let id : Int?
    
    init(id : Int) {
        self.id = id
    }
    
}
