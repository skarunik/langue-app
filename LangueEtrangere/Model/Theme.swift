//
//  Theme.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 06/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class Theme: NSObject, Codable {
    
    let id : Int?
    let name : String?
    let icon: String?
    let questions : [Question?]
    var answeredQuestionsCounter : Int = 0
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case icon
        case questions
    }
    
}
