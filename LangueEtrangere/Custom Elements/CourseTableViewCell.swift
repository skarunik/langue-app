//
//  CourseTableViewCell.swift
//  LangueEtrangere
//
//  Created by Nikita Shkarupa on 07/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import UIKit

class CourseTableViewCell: UITableViewCell {

    @IBOutlet weak var courseName: UILabel!
    @IBOutlet weak var courseLevel: UILabel!
    @IBOutlet weak var courseProgress: UILabel!
    @IBOutlet weak var courseFlag: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
