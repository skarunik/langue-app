//
//  ProfileTableViewController.swift
//  LangueEtrangere
//
//  Created by Nikita Shkarupa on 17/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController {
    
    let tableSections = [
        "Purchased courses",
        "Finished courses"
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return tableSections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.text = tableSections[section]
        label.backgroundColor = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
        label.layoutMargins.bottom = CGFloat(10.0)
        label.layoutMargins.top = CGFloat(10.0)
        label.layoutMargins.right = CGFloat(10.0)
        label.layoutMargins.left = CGFloat(10.0)
        return label
    }

}
