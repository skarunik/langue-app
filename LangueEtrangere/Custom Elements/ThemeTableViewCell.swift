//
//  ThemeTableViewCell.swift
//  LangueEtrangere
//
//  Created by Nikita Shkarupa on 08/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import UIKit

class ThemeTableViewCell: UITableViewCell {

    @IBOutlet weak var themeIcon: UIImageView!
    @IBOutlet weak var themeName: UILabel!
    @IBOutlet weak var themeProgress: UILabel!
    @IBOutlet weak var themeProgressBar: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
