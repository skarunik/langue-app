//
//  QuestionCollectionViewCell.swift
//  LangueEtrangere
//
//  Created by Nikita Shkarupa on 12/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import UIKit

class QuestionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var optionIcon: UIImageView!
    @IBOutlet weak var optionText: UILabel!
}
