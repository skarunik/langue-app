//
//  NewCourseCollectionViewCell.swift
//  LangueEtrangere
//
//  Created by Nikita Shkarupa on 09/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import UIKit

class NewCourseCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var courseTitle: UILabel!
    @IBOutlet weak var courseFlag: UIImageView!
    @IBOutlet weak var themesCount: UILabel!
    @IBOutlet weak var coursePrice: UILabel!

}
