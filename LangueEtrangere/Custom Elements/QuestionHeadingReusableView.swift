//
//  QuestionHeadingReusableView.swift
//  LangueEtrangere
//
//  Created by Nikita Shkarupa on 14/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import UIKit

class QuestionHeadingReusableView: UICollectionReusableView {
        
    @IBOutlet weak var questionText: UILabel!
    @IBOutlet weak var questionTip: UILabel!
    @IBOutlet weak var questionWord: UILabel!
    
}
