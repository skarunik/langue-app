//
//  PersonDataService.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 14/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class PersonDataService {
    
    private let updatePerson = "http://178.128.242.55/person/"
    private let personService = PersonService()
    private let courseService = CourseService()
    
    // add question to person's answered questions
    func addQuestion(personId : Int, courseId : Int, themeId : Int, questionId : Int, completion: @escaping (Bool, Error?) -> ()) -> Void {
        personService.getPersonById(id: personId, answeredQuestions: false) { (person, error) in
            self.courseService.getCourseById(id: courseId) { (course, err) in
               
                var check = false
                if course != nil {
                    for theme in course!.themes {
                        if theme!.id == themeId {
                            for question in theme!.questions {
                                if question!.id == questionId {
                                    check = true
                                    break
                                }
                            }
                            break
                        }
                    }
                }
                
                if person != nil && check {
                    var inArray = false
                    for personCourse in person!.courses {
                        if personCourse!.id == courseId {
                            for personTheme in personCourse!.themes {
                                if personTheme!.id == themeId {
                                    for question in personTheme!.answeredQuestions {
                                        if question!.id == questionId{
                                            inArray = true
                                            break
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if !inArray {
                        for course in person!.courses {
                            if course!.id == courseId {
                                var checkTheme = false
                                for theme in course!.themes{
                                    if theme!.id == themeId {
                                        theme!.answeredQuestions.append(AnsweredQuestion(id : questionId))
                                        checkTheme = true
                                    }
                                }
                                if !checkTheme {
                                    let newTheme = PersonTheme(id: themeId)
                                    newTheme.answeredQuestions.append(AnsweredQuestion(id: questionId))
                                    course!.themes.append(newTheme)
                                }
                            }
                        }
                        self.sendObject(person: person!) { (result, error) in
                            if error == nil {
                                completion(true, nil)
                                return
                            } else {
                                completion(false, error)
                                return
                            }
                        }
                    } else {
                        completion(false, error)
                        return
                    }
                } else {
                    completion(false, error)
                    return
                }
            }
        }
    }
    
    // add theme to person's themes
    func addTheme(personId : Int, courseId : Int, themeId : Int, completion: @escaping (Bool, Error?) -> ()) -> Void {
        personService.getPersonById(id: personId, answeredQuestions: false) { (person, error) in
            self.courseService.getCourseById(id: courseId) { (checkCourse, err) in
                
                var checkTheme = false
                if checkCourse != nil {
                    for theme in checkCourse!.themes {
                        if theme!.id == themeId {
                            checkTheme = true
                        }
                    }
                }
                
                if person != nil && checkTheme {
                    var inArray = false
                    for personCourse in person!.courses {
                        if personCourse!.id == courseId {
                            for personTheme in personCourse!.themes {
                                if personTheme!.id == themeId {
                                    inArray = true
                                    break
                                }
                            }
                        }
                    }
                    
                    if !inArray {
                        for course in person!.courses {
                            if course!.id == courseId {
                                course!.themes.append(PersonTheme(id : themeId))
                                break
                            }
                        }
                        self.sendObject(person: person!) { (result, error) in
                            if error == nil {
                                completion(true, nil)
                                return
                            } else {
                                completion(false, error)
                                return
                            }
                        }
                    } else {
                        completion(false, error)
                        return
                    }
                } else {
                    completion(false, error)
                    return
                }
            }
        }
    }
    
    // add course to person's courses
    func addCourse(personId : Int, courseId : Int, completion: @escaping (Bool, Error?) -> ()) -> Void {
        personService.getPersonById(id: personId, answeredQuestions: false) { (person, error) in
            
            if person != nil {
                var inArray = false
                for personCourse in person!.courses {
                    if personCourse!.id == courseId {
                        inArray = true
                    }
                }
                if !inArray {
                    person!.courses.append(PersonCourse(id : courseId))
                    self.sendObject(person: person!) { (result, error) in
                        print(result)
                        if error == nil {
                            completion(true, nil)
                            return
                        } else {
                            completion(false, error)
                            return
                        }
                    }
                } else {
                    completion(false, error)
                    return
                }
            }
        }
    }
    
    // send object to json server
    private func sendObject(person : Person, completion: @escaping (Bool, Error?) -> ()) -> Void {
        do {
            let json = try JSONEncoder().encode(person)
            if !json.isEmpty {
                var request = URLRequest(url: URL(string : self.updatePerson + String(person.id!))!)
                request.httpMethod = "PUT"
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                request.httpBody = json
                completion(false, nil)
                
                _ = URLSession.shared.dataTask(with: request) { (responseData: Data?, response: URLResponse?, error: Error?) in
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 201 {
                        completion(true, nil)
                        return
                    }
                }.resume()
            }
        } catch {
            completion(false, error)
            return
        }
    }
    
}
