//
//  ThemeService.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 09/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class ThemeService{
    
    private let personService = PersonService()
    private let courseService = CourseService()
    
    // add counter to each theme
    private func getThemesWithCounter(themes : [Theme?], personThemes : [PersonTheme?]) -> [Theme?] {
        for personTheme in personThemes {
            for theme in themes{
                if theme!.id! == personTheme!.id! {
                    theme!.answeredQuestionsCounter = personTheme!.answeredQuestions.count
                    break
                }
             }
          }
        return themes
    }
    
    // get themes by courseId and personIn
    // if third parameter is true - get themes with answeredQuestionsCounter
    func getThemesByCourseId(personId : Int, courseId : Int, counter : Bool, completion: @escaping ([Theme?], Error?) -> ()) -> Void {
        personService.getPersonById(id: personId, answeredQuestions : false) { (person, error) in
            self.courseService.getCourseById(id: courseId) { (course, error) in

                if course != nil && person != nil {
                    for personCourse in person!.courses {
                        if course!.id! == personCourse!.id! {
                            if counter {
                                completion(self.getThemesWithCounter(themes: course!.themes, personThemes: personCourse!.themes), nil)
                                return
                            } else {
                                completion(course!.themes, nil)
                                return
                            }
                        }
                    }
                    completion([], nil)
                    return
                } else {
                    completion([], nil)
                    return
                }
            }
            
        }
        
    }

    // get concrete theme by id
    func getThemeById(personId : Int, courseId : Int, themeId : Int, completion: @escaping (Theme?, Error?) -> ()) -> Void {
        self.getThemesByCourseId(personId: personId, courseId: courseId, counter: false) { (themes, error) in

            for theme in themes {
                if theme!.id == themeId {
                    completion(theme, error)
                    return
                }
            }
            completion(nil, error)
            return
        }
    
    }

}
