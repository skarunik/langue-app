//
//  LoginService.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 07/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class PersonService{
    
    private let byEmail = "http://178.128.242.55/person?email="
    private let byId = "http://178.128.242.55/person?id="
    private let createPerson = "http://178.128.242.55/person"
    
    // check login data
    func login(email : String, password : String, completion: @escaping (Int?, Error?) -> ()) -> Void {
        callAPIPerson(url : URL(string : self.byEmail + email)!) { (users, error) in
        
            if users.count >= 1{
                if password.elementsEqual(users.first!!.password!){
                    completion(users.first!!.id, nil)
                    return
                }
                else {
                    completion(nil, error)
                    return
                }
            } else {
                completion(nil, error)
                return
            }
            
        }
    }
    
    // check if email is not presented in db
     private func checkEmail(email : String, completion: @escaping (Bool, Error?) -> ()) -> Void {
        callAPIPerson(url : URL(string : self.byEmail + email)!) { (users, error) in
            
            if users.count == 0 {
                completion(true, nil)
                return
            } else {
                completion(false, error)
                return
            }
            
        }
    }

    // get concrete user by id
    func getPersonById(id : Int, answeredQuestions : Bool, completion: @escaping (Person?, Error?) -> ()) -> Void {
        callAPIPerson(url : URL(string: self.byId + String(id))!) { (users, error) in
        
            if users.count >= 1 {
                if answeredQuestions {
                    completion(self.addAnsweredQuestionsCounter(person: users.first!!), nil)
                    return
                } else {
                    completion(users.first!!, nil)
                    return
                }
                
            } else {
                completion(nil, error)
                return
            }
            
        }
    }
    
    // count all answered questions
    private func addAnsweredQuestionsCounter(person : Person) -> Person {
        for course in person.courses {
            for theme in course!.themes {
                person.answeredQuestions += theme!.answeredQuestions.count
            }
        }
        return person
    }
    
    // call json server to get users
    private func callAPIPerson(url : URL, completion: @escaping ([Person?], Error?) -> ()) -> Void {
        
        _ = URLSession.shared.dataTask(with: url){(data, response, error) in
            if error == nil {
                
                do {
                    let json = try JSONDecoder().decode([Person].self, from: data!)
                    completion(json, nil)
                    return
                } catch {
                    completion([], error)
                    return
                }
                
            } else {
                completion([], error)
                return
            }
        }.resume()
        
    }
    
    
    // call json server to save new user
    func registration(email: String, name : String, password : String, completion: @escaping (Bool, Error?) -> ()) -> Void {
        self.checkEmail(email: email) { (success, error) in
            if success {
                let person = Person(email : email, name : name, password : password)
                
                do {
                    let json = try JSONEncoder().encode(person)
                    if !json.isEmpty {
                        var request = URLRequest(url: URL(string : self.createPerson)!)
                        request.httpMethod = "POST"
                        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                        request.httpBody = json
                        
                        _ = URLSession.shared.dataTask(with: request) { (responseData: Data?, response: URLResponse?, error: Error?) in
                            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 201{
                                completion(true, nil)
                                return
                            }
                        }.resume()
                    }
                } catch {
                    completion(false, error)
                    return
                }
            } else {
                completion(false, error)
                return
            }
        }
    }
    
}
