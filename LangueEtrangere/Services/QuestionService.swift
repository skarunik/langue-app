//
//  QuestionService.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 09/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class QuestionService {
    
    private let personService = PersonService()
    private let themeService = ThemeService()
    
    // get questions with @param answered
    func getQuestionsByThemeId(personId : Int, courseId : Int, themeId : Int, completion: @escaping ([Question?], Error?) -> ()) -> Void {
        themeService.getThemeById(personId: personId, courseId: courseId, themeId: themeId) { (theme, error) in
            self.getPersonThemesById(personId: personId, courseId: courseId, themeId: themeId) { (personTheme, err) in

                if theme != nil {
                    if personTheme != nil {
                        for personQuestion in personTheme!.answeredQuestions {
                            for question in theme!.questions {
                                if question!.id == personQuestion!.id {
                                    question!.answered = true
                                    break
                                }
                            }
                        }
                    }
                    completion(theme!.questions, nil)
                    return
                } else {
                    completion([], error)
                    return
                }
            }
        }
    }
    
    // get personTheme with personAnsweredQuestions
     private func getPersonThemesById(personId : Int, courseId : Int, themeId : Int, completion: @escaping (PersonTheme?, Error?) -> ()) -> Void {
        personService.getPersonById(id: personId, answeredQuestions: false) { (person, error) in

            if person != nil {
                for personCourse in person!.courses {
                    if personCourse!.id == courseId {
                        for personTheme in personCourse!.themes {
                            if personTheme!.id == themeId {
                                completion(personTheme, nil)
                                return
                            }
                        }
                        completion(nil, error)
                        return
                    }
                }
            } else {
                completion(nil, error)
                return
            }
        }
    }
    
}
