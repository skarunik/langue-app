//
//  CourseService.swift
//  LangueEtrangere
//
//  Created by Tatiana Kulikova on 08/05/2019.
//  Copyright © 2019 T13. All rights reserved.
//

import Foundation

class CourseService {
    
    private let loginService = PersonService()
    private let all = "http://178.128.242.55/courses"
    private let byId = "http://178.128.242.55/courses?id="
    private let updateRating = "http://178.128.242.55/courses/"
    
    // get all person's courses by id
    // if second parameter is true - get available courses to person with concrete id
    // if third parameter is true - get courses with startedThemesCounter
    func getCourses(personId : Int, available : Bool, counter : Bool, completion: @escaping ([Course?], Error?) -> ()) -> Void {
        var courses : [Course?] = []
        loginService.getPersonById(id: personId, answeredQuestions : false) { (person, error) in
            self.getAllCourses() { (userCouses, err) in
                
                if person != nil {
                    for course in userCouses {
                        var inArray = false
                        for personCourse in person!.courses {
                            if personCourse!.id == course!.id {
                                inArray = true
                                break
                            }
                        }
                        if (available && !inArray) ||  (!available && inArray) {
                            courses.append(course)
                        }
                    }
                    
                    if counter && !available {
                        let coursesWithCounter = self.getCoursesWithCounter(courses: courses, personCouses: person!.courses)
                        completion(coursesWithCounter, nil)
                        return
                    } else {
                        completion(courses, nil)
                        return
                    }
                }
                else {
                    completion([], error)
                    return
                }
            }
        }
        
    }
    
    // get concrete course by id
    func getCourseById(id : Int, completion: @escaping (Course?, Error?) -> ()) -> Void {
        callAPICourses(url : URL(string: self.byId + String(id))!) { (courses, error) in
            
            if courses.count >= 1 {
                completion(courses.first!, nil)
                return
            } else {
                completion(nil, error)
                return
            }
            
        }
    }
    
    // get all courses with average rating
    func getAllCourses(completion: @escaping ([Course?], Error?) -> ()) -> Void {
        callAPICourses(url : URL(string: self.all)!) { (courses, error) in
            for course in courses {
                var counter: Float = 0
                for rating in course!.ratings {
                    counter += Float(rating!.rate!)
                }
                course!.rating = Float(course!.ratings.count == 0 ? 0 : counter / Float(course!.ratings.count))
            }
            completion(courses, nil)
            return
        }
    }
    
    // add counter to each course
    private func getCoursesWithCounter(courses : [Course?], personCouses : [PersonCourse?]) -> [Course?] {
        for personCouse in personCouses {
            for course in courses {
                if course!.id! == personCouse!.id! {
                    course!.startedThemesCounter = personCouse!.themes.count
                    break
                }
            }
        }
        return courses
    }
    
    // call json server to get courses
    private func callAPICourses(url : URL, completion: @escaping ([Course?], Error?) -> ()) -> Void {
        
        _ = URLSession.shared.dataTask(with: url){(data, response, error) in
            if error == nil {

                do {
                    let json = try JSONDecoder().decode([Course].self, from: data!)
                    completion(json, nil)
                    return
                } catch {
                    completion([], error)
                    return
                }
                
            } else {
                completion([], error)
                return
            }
        }.resume()

    }
    
    // add or update rating
    func addRating(personId : Int, courseId : Int, rate : Int, completion: @escaping (Bool, Error?) -> ()) -> Void {
        self.getCourseById(id: courseId) { (course, error) in
            if course != nil {
                var inArray = false
                for rating in course!.ratings {
                    if rating!.userId == personId {
                        inArray = true
                        rating!.rate = rate
                        break
                    }
                }
                if !inArray {
                    course!.ratings.append(Rating(userId: personId, rate: rate))
                }
                self.sendObject(course : course!) { (result, error) in
                    if error == nil {
                        completion(true, nil)
                        return
                    } else {
                        completion(false, error)
                        return
                    }
                }
            }  else {
                completion(false, error)
                return
            }
        }
    }
    
    // send object to json server
    private func sendObject(course : Course, completion: @escaping (Bool, Error?) -> ()) -> Void {
        do {
            let json = try JSONEncoder().encode(course)
            if !json.isEmpty {
                var request = URLRequest(url: URL(string : self.updateRating + String(course.id!))!)
                request.httpMethod = "PUT"
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                request.httpBody = json
                completion(false, nil)
                
                _ = URLSession.shared.dataTask(with: request) { (responseData: Data?, response: URLResponse?, error: Error?) in
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                        completion(true, nil)
                        return
                    }
                }.resume()
            }
        } catch {
            completion(false, error)
            return
        }
    }
    
}
